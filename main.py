import tkinter as tk
from tkinter import SOLID

buttons_text = [
    ["%", "CE", "C", "<--"],
    ["1/x", "x^2", "sqrt", "\\"],
    ["7", "8", "9", "*"],
    ["4", "5", "6", "-"],
    ["1", "2", "3", "+"],
    ["+/-", "0", ".", "="]
]

buttons_action = [
    ["", "", "", "backspace()"],
    ["", "", "", "type(\"/\")"],
    ["type(\"7\")", "type(\"8\")", "type(\"9\")", "type(\"*\")"],
    ["type(\"4\")", "type(\"5\")", "type(\"6\")", "type(\"-\")"],
    ["type(\"1\")", "type(\"2\")", "type(\"3\")", "type(\"+\")"],
    ["", "type(\"0\")", "type(\".\")", "solve()"]
]

window = tk.Tk()

window_width = int(window.winfo_screenwidth() // 4)
window_height = int(window.winfo_screenheight() // 1.5)


window.geometry(f"{window_width}x{window_height}")

window.title("Not Windows Calculator")

numbers = tk.StringVar()
field = tk.Entry(window, textvariable=numbers)
field.grid(columnspan=4, ipadx=70)

for i in range(6):
    for j in range(4):
        button1 = tk.Button(window, text=buttons_text[i][j], height=2, width=11, command=lambda: exec(buttons_action[i][j]))
        button1.grid(row=i + 1, column=j)

window.mainloop()